@extends('layouts.app')
@section('title','home')

@section('conteudo')



<div class="container">
    <article class="row">
        <div class="col-md-4">
            <img class="img-fluid" src="https:via.placeholder.com/400x250">
        </div>
        <div class="col-md-8">

            <h2>Notícias Destaque</h2>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio maxime voluptatum dicta facere
                minima repudiandae recusandae ratione nihil quam illo nesciunt assumenda tempora obcaecati, odio eaque
                molestias mollitia corporis? Neque.</p>
        </div>
    </article>
    <div class="row">
        @for($i =1; $i <= 3; $i++) 
        <div class="col-md-4 mt-5">

            <article class="card" a href="#">
                <img class="img-fluid" src="https:via.placeholder.com/500x250">
                <div class="card-body">
                    <h2 class="card-title">
                        <a href="#">Titulo Noticia</a></h2>
                    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat ab sint
                        modi,
                        aperiam, nihil rem fugiat explicabo atque architecto optio est quod doloribus dolore enim
                        illum,
                        eius tenetur alias qui!</p>


                </div>
                <div class="card-footer">
                    30/04/2019


                </div>

            </article>

        </div>
        @endfor
    </div>

</div>



@endsection
